/*
Healthcare OnDemand
Anderson Adams
Registration UI Class
Class will accept all regitration fields and then create user
 */
package com.revature.healthcareod.screens

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.revature.healthcareod.MainActivity
import com.revature.healthcareod.R
import com.revature.healthcareod.datahandlers.Users
import com.revature.healthcareod.viewmodels.LoginViewModel

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class SecondFragment : Fragment() {
    //nurse registration UI
    private lateinit var viewModel:LoginViewModel

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_registration, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel=ViewModelProvider(this).get(LoginViewModel::class.java)

        val myActivity: MainActivity = activity as MainActivity

        viewModel.nurseDataRepository = myActivity.getNursesDataRepository()

        //get all edit texts
        val firstName = view.findViewById<EditText>(R.id.SecondFragmentFirstName)
        val lastName = view.findViewById<EditText>(R.id.SecondFragmentLastName)
        val userName = view.findViewById<EditText>(R.id.SecondFragmentUserName)
        val password = view.findViewById<EditText>(R.id.SecondFragmentPassword)
        val phone = view.findViewById<EditText>(R.id.SecondFragmentPhone)
        val address = view.findViewById<EditText>(R.id.SecondFragmentAddress)
        view.findViewById<Button>(R.id.button_second).setOnClickListener {
            //register user
            viewModel.register(firstName.text.toString(), lastName.text.toString(), userName.text.toString(), password.text.toString(), phone.text.toString(), address.text.toString())
        }
        viewModel.navigateToFirstFragment.observe(viewLifecycleOwner, Observer<Boolean> { status ->
            if (status) {
                findNavController().navigate(R.id.action_SecondFragment_to_FirstFragment)
                viewModel.onNavigatedToFirstFragment()
            }
        })
        viewModel.toastString.observe(viewLifecycleOwner, Observer<String> { toast->
            Toast.makeText(this.requireContext(),toast,Toast.LENGTH_SHORT).show()
        })
    }
}