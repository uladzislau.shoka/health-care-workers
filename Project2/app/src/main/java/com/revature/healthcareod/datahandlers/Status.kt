package com.revature.healthcareod.datahandlers

enum class Status {
    Open, Accepted, Declined, Completed
}