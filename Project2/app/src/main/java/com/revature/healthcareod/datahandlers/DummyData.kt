package com.revature.healthcareod.datahandlers

import com.google.android.gms.maps.model.LatLng
import com.revature.healthcareod.database.jobdatafiles.JobData
import java.util.regex.Pattern

object DummyData {
    val dummyJH=ArrayList<Job>()

    fun getDummyJobs(): ArrayList<Job>{

        val dummyJobs = ArrayList<Job>()

        var temp = Job()
        temp.jobDescription="Nursing"
        temp.hospitalId=4
        temp.ratePerHour=15
        temp.totalAmount=120
        temp.status= "Open"
        temp.endDate= "04/23/2019"
        temp.jobName="Emergency Nurse Opening Yeet"
        temp.weeklySchedule = "1111100"
        temp.address = LatLng(29.7, -95.7)
        temp.textAddress = "1234 Hospital Road Atp 76."
        dummyJobs.add(temp)


        temp = Job()
        temp.jobDescription="Nursing"
        temp.hospitalId=5
        temp.ratePerHour=25
        temp.totalAmount=300
        temp.status= "Open"
        temp.endDate= "04/23/2024"
        temp.jobName="Emergency Nurse Opening"
        temp.weeklySchedule = "1111100"
        temp.address = LatLng(69.7, -195.7)
        temp.textAddress = "8987 South Brandywine Drive Parkville, MD 21234"
        dummyJobs.add(temp)


        temp = Job()
        temp.jobDescription="Nursing"
        temp.hospitalId=5
        temp.ratePerHour=25
        temp.totalAmount=250
        temp.status= "Open"
        temp.endDate= "04/23/2023"
        temp.jobName="Emergency Nurse Opening"
        temp.weeklySchedule = "1111100"
        temp.address = LatLng(129.7, -20.7)
        temp.textAddress = "8805 North Elmwood Lane Faribault, MN 55021"
        dummyJobs.add(temp)

        temp = Job()
        temp.jobDescription="Nursing"
        temp.hospitalId=5
        temp.ratePerHour=22
        temp.totalAmount=220
        temp.status= "Open"
        temp.endDate= "04/23/2022"
        temp.jobName="Emergency Nurse Opening"
        temp.weeklySchedule = "0011111"
        temp.address = LatLng(49.7, -95.7)
        temp.textAddress = "8805 North Elmwood Lane Faribault, MN 55021"
        dummyJobs.add(temp)


        temp = Job()
        temp.jobDescription="Nursing Pick up shift"
        temp.hospitalId=1
        temp.ratePerHour=25
        temp.totalAmount=250
        temp.status= "Open"
        temp.startDate="05/03/2021"
        temp.endDate= "05/04/2021"
        temp.jobName="Emergency Nurse Shift"
        temp.weeklySchedule = "0100000"
        temp.address = LatLng(22.75, -95.7)
        temp.textAddress = "8805 North Elmwood Lane Faribault, MN 55021"
        dummyJobs.add(temp)

        return dummyJobs
    }


    fun getHosNameBId(int:Int):String{
        return when(int){
            1->"Hochkis Hospital"
            2->"John's Hospital"
            3->"Hospital of Great Mercy"
            4->"Nurse Ratchet's Hospital"
            5->"Good Brothers Hospital"
            else->"Unknown Hospital"
        }
    }

    fun fillAllJobs (jobDataList: List<JobData>) {
        jobDataList.forEach { jobData ->
            var job = Job()
           // val p = Pattern.compile("\\d+")
          //  val doubleList = ArrayList<Double?>()
          //  var m = p.matcher(jobData.address)
          //  while (m.find()) {
           //     doubleList.add(m.group().toDoubleOrNull())
           // }
            val stringList = jobData.address.split(",")

            println("fill all jobs from database" + jobData.address + " " + stringList[0] + " " + stringList[1] )
            job.jobId = jobData.jobId
            job.hospitalId = jobData.hospitalId
            job.userName = jobData.userName
            job.ratePerHour = jobData.ratePerHour
            job.totalAmount = jobData.totalAmount
            job.jobName = jobData.jobName
            job.jobDescription = jobData.jobDescription
            job.startDate = jobData.startDate
            job.endDate = jobData.endDate
            job.status = jobData.status
            job.weeklySchedule = jobData.schedule

            job.address = LatLng(stringList[0].toDouble(), stringList[1].toDouble())
            job.textAddress = jobData.textAddress

            dummyJH.add(job)
        }
    }
}