package com.revature.healthcareod.viewmodels

import android.content.Context
import android.location.Geocoder
import android.text.Editable
import android.text.SpannableStringBuilder
import androidx.lifecycle.*
import com.google.android.gms.maps.model.LatLng
import com.revature.healthcareod.database.jobdatafiles.JobData
import com.revature.healthcareod.database.jobdatafiles.JobDataRepository
import com.revature.healthcareod.database.nursedatafiles.NurseData
import com.revature.healthcareod.database.nursedatafiles.NurseDataRepository
import com.revature.healthcareod.datahandlers.*
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.ArrayList

class MainViewModel : ViewModel() {
    val imgSrcUrl="https://images.unsplash.com/photo-1617932624157-d1963c3c9441?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Nnx8anBlZ3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
//    val imgSrcUrl="https://cdn.discordapp.com/attachments/824352669791289357/836363715351805982/photo-1579064211320-66c8ed1cd426.jpg"

    val blankedit: Editable = SpannableStringBuilder("")

    var nurseDataIsReaded = false
    var nurseDataRepository: NurseDataRepository? = null
    var nursesDataLiveData: LiveData<List<NurseData>>? = null

    var jobDataIsReaded = false
    var jobDataRepository: JobDataRepository? = null
    var jobsDataLiveData: LiveData<List<JobData>>? = null

    private var _navigateToJobHistory = MutableLiveData<Boolean>(false)
    val navigateToJobHistory: LiveData<Boolean>
        get() = _navigateToJobHistory
    private var _navigateToJobSearcher = MutableLiveData<Boolean>(false)
    val navigateToJobSearcher: LiveData<Boolean>
        get() = _navigateToJobSearcher
    private var _navigateToAccountview = MutableLiveData<Boolean>(false)
    val navigateToAccountview: LiveData<Boolean>
        get() = _navigateToAccountview
    private var _navigateToCurrentJobs = MutableLiveData<Boolean>(false)
    val navigateToCurrentJobs: LiveData<Boolean>
        get() = _navigateToCurrentJobs
    private var _navigateToSecondFragment = MutableLiveData<Boolean>(false)
    val navigateToSecondFragment: LiveData<Boolean>
        get() = _navigateToSecondFragment
    private var _navigateToFirstFragment = MutableLiveData<Boolean>(false)
    val navigateToFirstFragment: LiveData<Boolean>
        get() = _navigateToFirstFragment
    private var _navigateToFindJobs = MutableLiveData<Boolean>(false)
    val navigateToFindJobs: LiveData<Boolean>
        get() = _navigateToFindJobs
    private var _navigateToSchedule = MutableLiveData<Boolean>(false)
    val navigateToSchedule: LiveData<Boolean>
        get() = _navigateToSchedule
    private var  _navigateToAccountEdit= MutableLiveData<Boolean>(false)
    val navigateToAccountEdit: LiveData<Boolean>
        get() = _navigateToAccountEdit
    private var _edu = MutableLiveData<Boolean>(false)
    val edu: LiveData<Boolean>
        get() = _edu
    private var _passC = MutableLiveData<Int>(0)
    val passC: LiveData<Int>
        get() = _passC
    private var _pU = MutableLiveData<Int>(0)
    val pU: LiveData<Int>
        get() = _pU
    private var _job = MutableLiveData<Job>(null)
    val job: LiveData<Job>
        get() = _job

    private var _date = MutableLiveData<String>(null)
    val date: LiveData<String>
        get() = _date

    private var _jobFinderU = MutableLiveData<Boolean>(false)
    val jobFinderU: LiveData<Boolean>
        get() = _jobFinderU
    private var _currentJobsUpdated = MutableLiveData<Boolean>(false)
    val currentJobsUpdated: LiveData<Boolean>
        get() = _currentJobsUpdated
    private var _time = MutableLiveData<Long>(0)
    val time: LiveData<Long>
        get() = _time
    private var _locationChanged = MutableLiveData<Boolean>(false)
    val locationChanged: LiveData<Boolean>
        get() = _locationChanged

    private var _location = MutableLiveData<LatLng?>(null)
    val location: LiveData<LatLng?>
        get() = _location

    private var _acceptJobToast = MutableLiveData<Boolean>(false)
    val acceptJobToast: LiveData<Boolean>
        get() = _acceptJobToast



    var user: NurseAccount? = null
    val userName: String
        get() = user?.userName ?: "null"
    val firstName: String
        get() = user?.firstName ?: "null"
    val lastName: String
        get() = user?.lastName ?: "null"
    val address: String
        get() = user?.address ?: "null"

    val eduList: ArrayList<Degree>
        get() = user?.education ?: ArrayList<Degree>()

    fun navigateToJobHistory() {
        _navigateToJobHistory.value = true
    }

    fun onNavigatedToJobHistory() {
        _navigateToJobHistory.value = false
    }

    fun navigateToJobSearcher() {
        _navigateToJobSearcher.value = true
    }

    fun onNavigatedToJobSearcher() {
        _navigateToJobSearcher.value = false
    }

    fun navigateToAccountview() {
        _navigateToAccountview.value = true
    }

    fun onNavigatedAccountview() {
        _navigateToAccountview.value = false
    }

    fun navigateToCurrentJobs() {
        _navigateToCurrentJobs.value = true
    }

    fun onNavigatedCurrentJobs() {
        _navigateToCurrentJobs.value = false
    }

    fun navigateToSecondFragment() {
        _navigateToSecondFragment.value = true
    }

    fun onNavigatedToSecondFragment() {
        _navigateToSecondFragment.value = false
    }

    fun navigateToFirstFragment() {
        user = null
        _navigateToFirstFragment.value = true
    }

    fun onNavigatedToFirstFragment() {
        _navigateToFirstFragment.value = false
    }

    fun navigateToFindJobs() {
        _navigateToFindJobs.value = true
    }

    fun onNavigatedToFindJobs() {
        _navigateToFindJobs.value = false
    }

    fun navigateToSchedule() {
        _navigateToSchedule.value = true
    }

    fun onNavigatedToSchedule() {
        _navigateToSchedule.value = false
    }
    fun navigateToAccountEdit() {
        _navigateToAccountEdit.value = true
    }

    fun onNavigatedToAccountEdit() {
        _navigateToAccountEdit.value = false
    }

    fun onEduUpdated() {
        _edu.value = false
    }

    fun onPassCRead() {
        _passC.value = 0
    }

    fun onPURead() {
        _pU.value = 0
    }

    fun onJobFinderURead(){
        _jobFinderU.value=false
    }
    fun onCurrentJobsUpdated(){
        _currentJobsUpdated.value=false
    }



    fun login(userName: String, password: String) {
        val tempAccount: NurseAccount? = Users.login(userName, password)
        user = tempAccount
        navigateToJobSearcher()
    }

    fun saveEdu(s: String, d: String, dg: String, m: String):Int {
        if (s.trim() != "" && d.trim() != "" && dg.trim() != "" && m.trim() != "") {
            user?.let {
                it.education.add(Degree(dg, s, d, m))
                _edu.value = true
                //Log.e("EDU", "${it.education.size}")
                return 0
            }
        }
        return 1

    }

    fun savePass(p: String, p2: String) {
        user?.let {
            if (p != "") {
                if (p == p2) {
                    it.password = p
                    _passC.value = 1
                } else {
                    _passC.value = 2
                    //Toast.makeText(requireContext(), getString(R.string.error_message_unmatched), Toast.LENGTH_SHORT).show()
                }
            } else {
                _passC.value = 3
                //Toast.makeText(requireContext(), getString(R.string.blank_password), Toast.LENGTH_SHORT).show()
            }

            val nurseData = NurseData(it.firstName, it.lastName, it.userName, it.password, it.phone, it.address)
            updateNurseData(nurseData)
        }
    }

    fun submit(fName: String, lName: String, adrs: String) {
        user?.let {
            var scs = false
            if (fName.trim() != "" && lName.trim() != "" && fName != it.firstName && lName != it.lastName) {
                it.firstName = fName.trim()
                it.lastName = lName.trim()
//                binding.editTextAccViewFName.text= SpannableStringBuilder(fName)
//                binding.editTextAccViewLName.text= SpannableStringBuilder(lName)
                scs = true
            }
            if (adrs.trim() != "" && adrs != it.address) {
                it.address = adrs
//                binding.editTextAccViewAdrs.text= SpannableStringBuilder(adrs)
                scs = true
            }
            if (scs) {
                _pU.value = 1
                //Toast.makeText(requireContext(), getString(R.string.success_profile), Toast.LENGTH_SHORT).show()
            } else {
                _pU.value = 2
                //Toast.makeText(requireContext(), getString(R.string.error_message_nochanges), Toast.LENGTH_SHORT).show()
            }

            val nurseData = NurseData(it.firstName, it.lastName, it.userName, it.password, it.phone, it.address)
            updateNurseData(nurseData)
        }
    }


    fun initNursesData() {
        nursesDataLiveData = nurseDataRepository?.allNursesData?.asLiveData()
    }

    fun createJobsDataLiveData() {
        jobsDataLiveData = jobDataRepository?.allJobsData?.asLiveData()
    }

    fun initJobRows() = viewModelScope.launch {
        var jobsRows = jobDataRepository?.getNumOfJobsData()
        if (jobsRows == 0) {
            fillAllJobDataInDatabase()
        }
    }

    fun updateNurseData(nurseData: NurseData) = viewModelScope.launch {
        nurseDataRepository?.update(nurseData)
    }

    fun insertJobData(jobData: JobData) = viewModelScope.launch {
        jobDataRepository?.insert(jobData)
    }

    fun fillAllJobDataInDatabase() {

        for (job in DummyData.getDummyJobs()) {
            println("filling jobs " + job.address.toString())
            val string = job.address.latitude.toString() + "," + job.address.longitude.toString()
            val jobData = JobData(
                job.jobId,
                job.hospitalId,
                job.userName,
                job.ratePerHour,
                job.totalAmount,
                job.jobName ?: "null",
                job.jobDescription ?: "null",
                job.startDate ?: "null",
                job.endDate ?: "null",
                job.status ?: "null",
                job.weeklySchedule,
                address = string,
                job.textAddress
            )
            insertJobData(jobData)
        }
    }

    fun updateJobData(jobData: JobData) = viewModelScope.launch {
        jobDataRepository?.update(jobData)
    }


    fun setDateClicked(month: Int, day: Int, year: Int) {
        val job: Job = Job()
        job.jobName = "No Available Shift"
        _date.value = "${month + 1}/$day/$year"
        val calender: Calendar = Calendar.getInstance()
        calender.set(year, month, day)
        val dayOfWeek: Int = calender.get(Calendar.DAY_OF_WEEK)

        //check if job exist, if not return empty job
        if (user!!.getJobDayOfWeek(dayOfWeek, day, month + 1, year) != null) {
            println("not null setting job")
            _job.value = user!!.getJobDayOfWeek(dayOfWeek, day, month  + 1, year)
        } else {
            println("null setting empty job")
            _job.value = job
        }
    }

    fun setDateSearched(date: String) {
        var parts = date.split("/")
        val month = parts[0].toInt() - 1
        val day = parts[1].toInt()
        val year = parts[2].toInt()
        val calender: Calendar = Calendar.getInstance()
        calender.set(Calendar.YEAR, year)
        calender.set(Calendar.MONTH, month)
        calender.set(Calendar.DAY_OF_MONTH, day)
        val millitime = calender.timeInMillis
        _time.value = millitime
    }

    fun setCurrentDate(currentTimeMilli: Long) {
        val calender: Calendar = Calendar.getInstance()
        calender.timeInMillis = currentTimeMilli
        _time.value = currentTimeMilli
        _date.value =
            "${calender.get(Calendar.MONTH)}" + "/" + "${calender.get(Calendar.DAY_OF_MONTH)} " + "/" + "${
                calender.get(Calendar.YEAR)
            }"
    }

    val jobHistory: List<Job>
        get() = run {
            user?.let {
                return it.getJobHistory()
            }
            return ArrayList<Job>()
        }

    // this is complicated and was a pain to do correctly, it sums
    val earnings: String
        get() = run {
            try {
                var dt = 0
                for (j in user!!.getJobHistory()) {
                    if (j.status==Status.Completed.name) {
                        dt += j.totalAmount
                    }
                }
                return dt.toString()
            } catch (e: Exception) {
                return "0,error"
            }
        }
    val allOpenJobs: ArrayList<Job>
        get() = run {
            user?.let {
                return it.getAllOpenJobs()
            }
            return ArrayList<Job>()
        }

    val allCurrentJobs: ArrayList<Job>
        get() = run {
            user?.let {
                return it.getAllAcceptedJobs()
            }
            return ArrayList<Job>()
        }

    fun acceptJob(job: Job) {
        println("accept job ${job.weeklySchedule} ${job.jobName}")
        if(user?.checkJobAvalible(job) == true) {
            val acceptedJob = user?.acceptJob(job)
            acceptedJob?.let {
                val string = job.address.latitude.toString() + "," + job.address.longitude.toString()
                var jobData = JobData(
                    it.jobId,
                    it.hospitalId,
                    it.userName,
                    it.ratePerHour,
                    it.totalAmount,
                    it.jobName?:"null",
                    it.jobDescription?:"null",
                    it.startDate?:"null",
                    it.endDate?:"null",
                    it.status?:"null",
                    it.weeklySchedule,
                    address = string,
                    it.textAddress
                )
                updateJobData(jobData)
            }
        }
        else{
           _acceptJobToast.value = true
        }
//        jobHistory?.add(job)
//        job.status = "Accepted"
//        job.userName=user?.userName?:""

        //Anderson - I commented this out because if we remove the job then the current jobs view can not longer see the job, dummy data is the list of all jobs
      //  DummyData.dummyJH.remove(job)
        //if (user != null) {}// && tempUser.checkJobAvalible(job)) {
            //tempUser.addJob(job)
            //dataset[position].userName = tempUser.userName
            // dataset[position].status = "Accepted"
//            updateJobDataFunction.invoke(JobData(
//                dataset[position].jobId,
//                dataset[position].hospitalId,
//                dataset[position].userName,
//                dataset[position].ratePerHour,
//                dataset[position].totalAmount,
//                dataset[position].jobName?:"null",
//                dataset[position].jobDescription?:"null",
//                dataset[position].startDate?:"null",
//                dataset[position].endDate?:"null",
//                dataset[position].status?:"null"))
//            dataset.remove(dataset[position])
//            notifyDataSetChanged()
//        }else{
//            Toast.makeText(context, "Can not accept job", Toast.LENGTH_LONG)
//        }
//        */
        _jobFinderU.value=true
    }



    fun removeJob(job: Job) {
        val removedJob = user?.declineJob(job)
        removedJob?.let {
            val string = job.address.latitude.toString() + "," + job.address.longitude.toString()
            var firstJobData = JobData(
                it.first.jobId,
                it.first.hospitalId,
                it.first.userName,
                it.first.ratePerHour,
                it.first.totalAmount,
                it.first.jobName?:"null",
                it.first.jobDescription?:"null",
                it.first.startDate?:"null",
                it.first.endDate?:"null",
                it.first.status?:"null",
                it.first.weeklySchedule,
                address = string,
                it.first.textAddress
            )
            updateJobData(firstJobData)

            var secondJobData = JobData(
                0,
                it.second.hospitalId,
                it.second.userName,
                it.second.ratePerHour,
                it.second.totalAmount,
                it.second.jobName?:"null",
                it.second.jobDescription?:"null",
                it.second.startDate?:"null",
                it.second.endDate?:"null",
                it.second.status?:"null",
                it.second.weeklySchedule ,
                address = string,
                it.second.textAddress
            )
            insertJobData(secondJobData)
        }



       /*
//        jobHistory?.remove(job)
//        job.userName=""
//
//        //changed to open from declined to get functional accept and decline, will ask how job history works and see how i can fix :D
//        job.status= "Open"
       // DummyData.dummyJH.add(job)
//        dataset[position].userName = " "
//        dataset[position].status = "Open"
//        updateJobDataFunction.invoke(JobData(
//            dataset[position].jobId,
//            dataset[position].hospitalId,
//            dataset[position].userName,
//            dataset[position].ratePerHour,
//            dataset[position].totalAmount,
//            dataset[position].jobName?:"null",
//            dataset[position].jobDescription?:"null",
//            dataset[position].startDate?:"null",
//            dataset[position].endDate?:"null",
//            dataset[position].status?:"null"))
//        dataset.remove(dataset[position])
//        notifyDataSetChanged()
        */

        _currentJobsUpdated.value=true
    }


    fun completeJob(job:Job){
        val completedJob = user?.completeJob(job)
        completedJob?.let {
            val string = job.address.latitude.toString() + "," + job.address.longitude.toString()
            var firstJobData = JobData(
                it.first.jobId,
                it.first.hospitalId,
                it.first.userName,
                it.first.ratePerHour,
                it.first.totalAmount,
                it.first.jobName?:"null",
                it.first.jobDescription?:"null",
                it.first.startDate?:"null",
                it.first.endDate?:"null",
                it.first.status?:"null",
                it.first.weeklySchedule,
                address = string,
                it.first.textAddress
            )
            updateJobData(firstJobData)

            var secondJobData = JobData(
                0,
                it.second.hospitalId,
                it.second.userName,
                it.second.ratePerHour,
                it.second.totalAmount,
                it.second.jobName?:"null",
                it.second.jobDescription?:"null",
                it.second.startDate?:"null",
                it.second.endDate?:"null",
                it.second.status?:"null",
                it.second.weeklySchedule,
                address = string,
                it.second.textAddress
            )
            insertJobData(secondJobData)
        }


        _currentJobsUpdated.value=true
    }

    fun openMap(latlng: LatLng){
        _locationChanged.value = true
        _location.value = latlng

    }

    fun closeMap(){
        _locationChanged.value = false
    }

    fun setAcceptToastFalse(){
        _acceptJobToast.value = false
    }

}