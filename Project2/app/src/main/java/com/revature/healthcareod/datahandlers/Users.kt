/*
Healthcare OnDemand
Anderson Adams
Users Class
Class will hold main list of customers, transactions, and registration
 */

package com.revature.healthcareod.datahandlers

import android.content.Context
import com.revature.healthcareod.MainActivity
import com.revature.healthcareod.database.nursedatafiles.NurseData
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.*

object Users {
//    init {
//        register("Example","Customer","a","a","","")
//    }

    private val file: String = "myUsers.txt"
    //static object holding customer accounts and transactions
    var customers : HashMap<String, NurseAccount> = HashMap<String, NurseAccount> ()
    var transactions : ArrayList<String> = ArrayList<String>()

    fun getUser(userName: String): NurseAccount?{
        if (userName=="null"){
            return null
        }
        return customers[userName]
    }

    //registration function will make sure first name, last, user, and password are
    fun register(firstName: String, lastName: String, userName: String, password: String, phone: String, address: String):String{

        if(customers.containsKey(userName)){
//            val toast = Toast.makeText(context, "Account already registered", Toast.LENGTH_SHORT)
//            toast.show()
            return "taken"
        }//if account exists

        if(firstName.isEmpty() || firstName == "null") {
//            val toast = Toast.makeText(context, "Incorrect First name", Toast.LENGTH_SHORT)
//            toast.show()
            return "namef empty"
        }//if first name is correctly entered
        if(lastName.isEmpty() || lastName == "null") {
//            val toast = Toast.makeText(context, "Incorrect Last name", Toast.LENGTH_SHORT)
//            toast.show()
            return "namel empty"
        }//if last name is correctly entered
        if(userName.isEmpty() || userName == "null") {
//            val toast = Toast.makeText(context, "Incorrect User name", Toast.LENGTH_SHORT)
//            toast.show()
            return "username empty"
        }//if user name is correctly entered
        if(password.isEmpty() || password == "null") {
//            val toast = Toast.makeText(context, "Incorrect password", Toast.LENGTH_SHORT)
//            toast.show()
            return "passwordempty"
        }//if password is correctly entered

        val tempUser = NurseAccount(firstName, lastName, userName, password, phone, address) //create temp Nurse account
        customers[userName] = tempUser //create user

        transactions.add("Registered Account: ${tempUser.firstName}, Date: ")

//        val toast = Toast.makeText(context, "Account registered, proceeded to login.", Toast.LENGTH_SHORT)
//        toast.show()
        return "registered"
    }//function register new nurse

    fun login(userName: String, password: String): NurseAccount?{

        return if(customers[userName] != null && customers[userName]!!.password == password){ //if user exists and the password entered matches
            customers[userName]

        } else{
            null
        }
    }//function login

    fun fillAllNurses (nurseDataList: List<NurseData>) {
        nurseDataList.forEach { nurseData ->
            customers[nurseData.userName] = NurseAccount(
                nurseData.firstName,
                nurseData.lastName,
                nurseData.userName,
                nurseData.password,
                nurseData.phone,
                nurseData.address)
        }
    }
}