package com.revature.healthcareod.screens

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.model.LatLng
import com.revature.healthcareod.R
import com.revature.healthcareod.databinding.FragmentScheduleBinding
import com.revature.healthcareod.viewmodels.MainViewModel


class ScheduleFragment : Fragment() {
    private val viewModel: MainViewModel by activityViewModels()

    private lateinit var _binding: FragmentScheduleBinding
    private val binding get() = _binding!!
    lateinit var userName: String

    override fun onCreate(savedInstanceState: Bundle?) {
        println("Schedule made")
        super.onCreate(savedInstanceState)

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        println("in Create View")

        _binding = FragmentScheduleBinding.inflate(inflater, container, false)
        println("binding made")
        binding.lifecycleOwner = this
        println("Life Cycle owner set")
        val view = binding.root
        binding.calendarView.setDate(System.currentTimeMillis(), true, true)

        //view bindings
        binding.shLocationBtn.setBackgroundColor(Color.TRANSPARENT)


        //set up live data

        //Listeners
        binding.calendarView.setOnDateChangeListener{calander, year, month, day ->
            viewModel.setDateClicked(month, day, year)
        }

        binding.btnSearchDate.setOnClickListener {
            viewModel.setDateSearched(binding.shDate.text.toString())
        }

        binding.btnSearchCurrentDate.setOnClickListener {
            binding.calendarView.setDate(System.currentTimeMillis(), true, true)
            viewModel.setCurrentDate(System.currentTimeMillis())
        }

        //Observers

        //view if date changed when calander clicked. Change date text to clicked date
        viewModel.date.observe(viewLifecycleOwner, { date ->
            binding.shDate.setText(date)
        })

        //view if time data changed, this is millisec time to set calander date to entered date
        viewModel.time.observe(viewLifecycleOwner, { time ->
            binding.calendarView.setDate(time, true, true)
        })

        //check if job to be viewed is chnaged
        viewModel.job.observe(viewLifecycleOwner, { job ->
            if(job != null){

                //val yeet = viewModel.convertAddress(job.address, requireContext())

                println(job.jobName + " " + job.jobDescription + " " + job.address)
                binding.shJobNameTv.text = job.jobName
                binding.shHospitalNameTv.text = job.getHosNameBId
                binding.shDescriptionTv.text = job.jobDescription
                (job.ratePerHour.toString() + " $/hr").also { binding.shRateTv.text = it }

              //  binding.shLocationBtn.text = yeet

                "${job?.startTime}hr to ${job.endTime}hr".also { binding.shHoursTv.text = it }
                binding.shLocationBtn.text = job.textAddress
                binding.shLocationBtn.setOnClickListener{
                    job.address?.let { it1 -> viewModel.openMap(it1) }
                }
            }

        })

        viewModel.navigateToJobSearcher.observe(viewLifecycleOwner, { status ->
            if (status) {
                findNavController().navigate(R.id.action_schedule_to_jobSearcher)
                viewModel.onNavigatedToJobSearcher()
            }
        })

        viewModel.locationChanged.observe(viewLifecycleOwner,Observer<Boolean>{ status->
            if(status) {
                findNavController().navigate(R.id.action_schedule_to_maps)
                viewModel.closeMap()
            }
        })

        return view
    }

}