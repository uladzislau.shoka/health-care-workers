package com.revature.healthcareod.screens

import android.Manifest
import android.content.pm.PackageManager
import androidx.fragment.app.Fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.activity.addCallback
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.revature.healthcareod.R
import com.revature.healthcareod.viewmodels.MainViewModel

 class MapsFragment() : Fragment(){//, OnMapReadyCallback{
    private val viewModel: MainViewModel by activityViewModels()

    private val REQUEST_LOCATION_PERMISSION = 1

    private lateinit var map: GoogleMap

    private val callback = OnMapReadyCallback { googleMap ->
        /**
         * Manipulates the map once available.
         * This callback is triggered when the map is ready to be used.
         * This is where we can add markers or lines, add listeners or move the camera.
         * In this case, we just add a marker near Sydney, Australia.
         * If Google Play services is not installed on the device, the user will be prompted to
         * install it inside the SupportMapFragment. This method will only be triggered once the
         * user has installed Google Play services and returned to the app.
         */
        map = googleMap
        enableMyLocation()

         googleMap.addMarker(MarkerOptions().position(viewModel.location.value!!).title(viewModel.job.value?.getHosNameBId))
         googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(viewModel.location.value!!, 10f))

    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_maps, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            findNavController().navigateUp()
        }

    }

   // override fun onMapReady(p0: GoogleMap) {
  //      map = p0
   //     map.addMarker(MarkerOptions().position(viewModel.location.value!!).title(viewModel.job.value?.getHosNameBId))
   //     map.moveCamera(CameraUpdateFactory.newLatLng(viewModel.location.value!!))
   //     enableMyLocation()

        //requireActivity().onBackPressedDispatcher.addCallback(this) {
        //    findNavController().navigate(R.id.action_maps_to_findJobs)

            //println("yeeet")
            // println(findNavController().currentBackStackEntry)
            //  when(parentFragment){
            //     FindJobs::class -> {findNavController().navigate(R.id.action_maps_to_findJobs); println("yeet")}
            //     CurrentJobs::class -> findNavController().navigate(R.id.action_maps_to_currentJobs)
            //      ScheduleFragment::class -> findNavController().navigate(R.id.action_maps_to_schedule)
            //  }

       // }

   // }

    private fun isPermissionGranted():Boolean{
        return ContextCompat.checkSelfPermission( requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if(requestCode == REQUEST_LOCATION_PERMISSION){
            if(grantResults.contains(PackageManager.PERMISSION_GRANTED)){
                enableMyLocation()
            }
        }
    }

    private fun enableMyLocation(){

        if (ContextCompat.checkSelfPermission(requireContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) ==
            PackageManager.PERMISSION_GRANTED &&
            ContextCompat.checkSelfPermission(requireContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
            PackageManager.PERMISSION_GRANTED) {
            map.isMyLocationEnabled = true;
        } else {
           println("Permission denied")
        }


    }//fun enable location

}