/*
Healthcare OnDemand
Anderson Adams
Job finder Class
Class will create individual views for recycler view. Hold all job info and accept button
 */
package com.revature.healthcareod.datahandlers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.revature.healthcareod.R
import com.revature.healthcareod.database.jobdatafiles.JobData
import com.revature.healthcareod.databinding.EduElementBinding
import com.revature.healthcareod.databinding.JobFinderElementBinding
import com.revature.healthcareod.viewmodels.MainViewModel

class JobFinder( private val dataset: ArrayList<Job>, private val viewModel:MainViewModel):RecyclerView.Adapter<JobFinder.ItemViewHolder>()  {
//    private val tempUser = Users.getUser(userName)//temp user


    //holder text views and accept button
    class ItemViewHolder(private var binding: JobFinderElementBinding):RecyclerView.ViewHolder(binding.root){
        fun bind(job: Job,viewModel: MainViewModel){
            println(job.address)
            binding.job=job
            binding.viewModel=viewModel
            binding.testinclude.job=job
            binding.jhLocationButton.text = job.textAddress
            binding.jhLocationButton.setOnClickListener {
                job.address?.let { it1 -> viewModel.openMap(it1) }
            }
            binding.executePendingBindings()
        }
    }

    //inflate adapter holding job finder views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val adapterLayout= LayoutInflater.from(parent.context).inflate(R.layout.job_finder_element,parent,false)
        return ItemViewHolder(JobFinderElementBinding.inflate(LayoutInflater.from(parent.context)))
    }

    //set all text data and listen for accept button at each job in list
    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(dataset[position],viewModel)

//        holder.h.text=DummyData.getHosNameBId(job.hospitalId)
//        holder.jn.text=job.jobName
//        holder.d.text=job.jobDescription
//        holder.r.text=job.ratePerHour.toString()
//        holder.t.text=job.totalAmount.toString()
//        holder.accept.setOnClickListener{
//            if (tempUser != null ){// && tempUser.checkJobAvalible(job)) {
//                tempUser.addJob(job)
//                dataset[position].userName = tempUser.userName
//                dataset[position].status = "Accepted"
//            updateJobDataFunction.invoke(JobData(
//                     dataset[position].jobId,
//                     dataset[position].hospitalId,
//                     dataset[position].userName,
//                     dataset[position].ratePerHour,
//                     dataset[position].totalAmount,
//                     dataset[position].jobName?:"null",
//                     dataset[position].jobDescription?:"null",
//                     dataset[position].startDate?:"null",
//                     dataset[position].endDate?:"null",
//                     dataset[position].status?:"null"))
//                dataset.remove(dataset[position])
//                notifyDataSetChanged()
//            }else{
//                Toast.makeText(context, "Can not accept job", Toast.LENGTH_LONG)
//            }
//        }

    }

    override fun getItemCount(): Int {
        return dataset.size
    }

}