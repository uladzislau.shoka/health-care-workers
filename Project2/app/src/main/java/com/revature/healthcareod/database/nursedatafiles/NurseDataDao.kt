package com.revature.healthcareod.database.nursedatafiles

import androidx.room.*
import com.revature.healthcareod.database.jobdatafiles.JobData
import kotlinx.coroutines.flow.Flow


@Dao
interface NurseDataDao {

    @Query("SELECT * FROM nurseDataTable")
    fun getAllNursesData(): Flow<List<NurseData>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(nurseData: NurseData)

    @Query("DELETE FROM nurseDataTable")
    suspend fun deleteAll()

    @Update
    suspend fun updateNurseData(nurseData: NurseData?): Int
}