package com.revature.healthcareod.datahandlers

import com.google.android.gms.maps.model.LatLng
import com.revature.healthcareod.datahandlers.Status

class Job {
    var jobId = 0
    var hospitalId = 0
    var userName = ""
    var ratePerHour = 0
    val stringRatePerHour:String
    get()= ratePerHour.toString()
    var totalAmount = 0
    val stringTotalAmount:String
    get()=totalAmount.toString()
    var jobName: String? = null
    var jobDescription: String? = null
    var startDate: String? = null
    var endDate: String? = null
    var address: LatLng = LatLng(0.0,0.0)
    var textAddress: String = "No Address Given"
    var startTime: Double = 9.00
    val stringST:String
    get()=startTime.toString()
    var endTime: Double = 17.00
    val stringET:String
        get()=endTime.toString()
    var weeklySchedule: String = "0000000"

    var status: String? = null
        set(status) {
            val status = Status.valueOf(status!!)
            field = status.name
        }
    val getHosNameBId:String
    get()=
        when(hospitalId){
            1->"Hochkis Hospital"
            2->"John's Hospital"
            3->"Hospital of Great Mercy"
            4->"Nurse Ratchet's Hospital"
            5->"Good Brothers Hospital"
            else->"Unknown Hospital"

    }
}