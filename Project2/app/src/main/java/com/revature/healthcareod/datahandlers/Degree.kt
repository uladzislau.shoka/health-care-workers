package com.revature.healthcareod.datahandlers

data class Degree(val level:String,val school:String,val gradDate:String,val major:String)
